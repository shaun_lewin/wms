var mongoose = require('mongoose');
var User = require('./user');
var Item = require('./item');
var $_ = require('underscore');

var exchangeSchema = new mongoose.Schema({

    //Properties
    lender: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
    },
    borrower: {
      type: mongoose.Schema.Types.ObjectId,
      required: true
    },
    loanDate: {
      type: Date,
      required: true,
    },
    item: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
    },
    type: {
      type: String,
      enum: ['loan','return']
    },
    exchangeRating: {
      type: Number,
      min: 1,
      max: 5
    },
    length: {
      type: Number
    },
    review: {
      type: String
    }
});

exchangeSchema.methods.getModel = function() {
  return this.model('Exchange');
};

exchangeSchema.methods.process = function(req,res,next) {
  console.log("processing exchange");

  //Check if lender is the borrower
  if(this.lender.id == this.borrower.id) {
    return next('Exchange Error: Lender can\'t be borrower');
  }

  var _users,
      _lender,
      _borrower,
      _exchangeItem,
      _exchange = this,
      _exchangeItemIndex,
      _readyToExchange = false,

  _users = User.where('_id').in([_exchange.lender, _exchange.borrower]).exec();

  _users.then(function(users){

      //Check if two users returned
      if(users.length != 2) {
          return next('Exchange Error: Both users not found');
      }

      //Get lender and borrower from result
      if(users[0].id == _exchange.lender) {
          _lender   = users[0];
          _borrower = users[1];
      } else {
          _lender  = users[1];
          _borrower = users[0];
      }

      if(typeof _lender == 'object' && typeof _borrower == 'object') {
        readyToExchange = true;
      }
  })

  //Find item in lender items
  //.id converts mongoose ID Object to string
  .then(function() {
    if(!readyToExchange) {
      return next('Exchange Error: Exchange not ready!');
    }

    exchangeItemIndex = $_.findIndex(_lender.items, function(item) {
        return item._id.id == _exchange.item.id;
    });

    if(exchangeItemIndex == -1) {
      next('User item not found');
    }

    _exchangeItem =  _lender.items[exchangeItemIndex];
  })

  /*
  Update loaned lenders' item
  */
  .then(function() {
      User.findOneAndUpdate(
        {
          "_id": _lender,
          "items._id": _exchange.item
        },
        {
          "$set" : {
              "items.$.borrowed" : true,
              "items.$.owned" : true
          }
        },
        function(err,doc) {
          if(err) return next(err);

          doc.save(function(err) {
              console.log('_lender saved');
              if(err) return next(err);
          });
        }
      );
  })

  .then(function() {
    var exchangeItemTmp2 = _exchangeItem;
    exchangeItemTmp2.owned = false;
    exchangeItemTmp2.borrowed = false;

    _borrower.items.push(exchangeItemTmp2);
    _borrower.save(function(err){
        console.log('_borrower saved');
        if(err) return next(err);

        res.json({
          success: true
        });
    });

    console.log("processing finished");
  });

};

var Exchange = mongoose.model('Exchange', exchangeSchema);

module.exports = Exchange;
