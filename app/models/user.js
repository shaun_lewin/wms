//app/models/user.js
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var itemSchema = require('./item').schema;

var userSchema = new Schema({
	username: {
		type: String,
		required: true,
		unique: true
	},
	email: {
		type: String,
		required: true,
		//todo - email validation module/funcion
	},
	password: {
		type: String,
		minlength: 5,
		required: true
	},
	dateAdded: {
		type: Date,
		default: Date.now(),
	},
	items:  [itemSchema],
	apiToken: {
		type: String
	},
	rating: {
		type: Number,
		min: 0,
		max: 5,
		default: 0
	}
});

userSchema.methods.getRating = function() {
		if(this.rating == 0) {
				return null;
		}
		return this.rating;
}

//Validates email
userSchema.path('email').validate(function (email) {
   var emailRegex = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
   return emailRegex.test(email);
}, 'Invalid email.')

var User = mongoose.model('User', userSchema);



module.exports = User;
