//app/models/item.js
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var itemSchema = new Schema({
	id: {
		type: mongoose.Schema.Types.ObjectId,
	},
	name: {
		type: String,
		required: true
	},
	borrowed: {
		type: Boolean,
		default: false
	},
	imageUrl: {
		type: String
	},
	description: {
		type: String,
	},
	owned: {
		type: Boolean
	}
});

var Item = mongoose.model('Item', itemSchema);

module.exports = Item;
