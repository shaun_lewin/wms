//main.js

//Packages
var express    = require('express'),
		app 			 = express(),
		morgan 		 = require('morgan'),
		bodyParser = require('body-parser'),
		config 		 = require('./config');

//Connect to database
var mongoose 	= require('mongoose'),
 		connected = mongoose.connect('mongodb://localhost'),
 		db 				= mongoose.connection;
 		port 			= 3000;

db.on('error', console.error.bind(console, 'Connection error:'));

//Routers
var apiRouter = express.Router();

//Routes
var user = require('./routes/user'),
		item = require('./routes/item'),
		exchange = require('./routes/exchange');

//Authentication route
apiRouter.post('/authenticate', require('./routes/authenticate'));

//Verify all api request
apiRouter.use(require('./routes/verify-token'));

//User endpoints
apiRouter.route('/user')
.get(user.getUsers)
.post(user.createUser)
.put(user.updateUser);

//Item endpoints
apiRouter.route('/item')
.post(item.addItem)
.get(item.getItems)
.put(item.removeItem)
.delete(item.updateItem);

//Exchange endpoints
apiRouter.route('/exchange')
.post(exchange.addExchange)
.get(exchange.getExchange)
.put(exchange.updateExchange);
//.delete(exchange.removeExchange);

//API Error Handler
apiRouter.use(function(err,req,res,next) {
		console.log(err);
		res.status(404).json({'error: ': err});
});

//Add middleware to app
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
app.use(morgan('common'));
app.use('/api', apiRouter);
app.listen(port);

console.log("running.....");
