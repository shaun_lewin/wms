var mongoose = require('mongoose');
var User = require('../app/models/user');

module.exports =  {

  addItem: function(req,res,next) {
      var user_query = User.findOne({"_id": req.userId}).exec();

      user_query.then(function(user) {

        var item = {
          name: req.body.name,
          description: req.body.description,
          borrowed: req.body.borrowed,
          imageUrl: req.body.imgUrl,
          quantity: req.body.quantity
        };

        user.items.push(item);

        user.save(function(err) {
            if(err) res.send(err);

            res.json({
              success: true,
              message: "Item added to trunk"
            });
        });

      });
  },

  getItems: function(req,res,next) {
      User.findOne({"_id": req.userId}, function(err, user) {
          if(err) throw err;

          res.json(user.items);
      });
  },

  removeItem: function(req,res,next) {
     User.findOne({"_id": req.userId}, function(err, user) {

        var itemId = req.body.itemId, removed;
        for(var i=0; i < user.items.length; i++) {
            if(user.items[i]._id == itemId) {
                removed = user.items.splice(i,1);
                break;
            }
        }

        user.save(function(err) {
            if(err) res.send(err);

            res.json({
              success: true,
              message: "Removed: " + removed
            });
        });
     });
  },

  updateItem: function(req,res,next) {

      var values = {};
      for(var key in req.body) {
          values["items.$." + key] = req.body[key];
      }

      User.findOneAndUpdate({
        "_id": req.userId,
        "items._id": req.body.itemId
      },
      {
        "$set": values
      },
      function(err, doc) {
        if(err) throw err;

        doc.save(function(err) {
          if(err) throw err;

          res.json({
            success: true,
            message: "Item updated!"
          });
        });

      });
  },
}
