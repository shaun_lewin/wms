var User = require('../app/models/user'),
		jwt = require('jsonwebtoken'),
		secret = require('../config').secret;

module.exports = function(req,res,next) {

	var user_query = User.findOne({username: req.body.username}).exec();

	user_query.then(function(user) {

			if (!user) {
				return next('User not found');
			}

			if (user.password != req.body.password) {
					return next('Wrong password!');
			}

			//Save api token to user schema
			if(user.apiToken && user.apiToken.length) {
					return next('User already authenticated!');
			}

		 	var token = jwt.sign({_id: user._id}, secret, {
					expiresIn: 2592000
			});

			user.apiToken = token;

			user.save(function(err) {
				if (err) {
					return next(err);
				}
			});

			res.json({
				success: true,
				token: token
			});
		});
};
