var jwt    = require('jsonwebtoken'),
    secret = require('../config').secret,
    User   = require('../app/models/user');

module.exports = function (req,res,next) {
    var token = req.headers['x-access-token'];

    //Skip valiation for user creation
    //TODO - figure out better way to do this.
    if(req.method == 'POST' && req.url == '/user') {
       return next();
    }

		//Verify token
		jwt.verify(token, secret, function(err, decoded) {
  		if(err) return next(err);

      User.findOne({"apiToken": token}, function(err, currentUser) {
        if(err) return next(err);

        if(currentUser != null) {
          req.userId = currentUser._id;
          req.decoded = decoded;
          return next();
        }

        next('User not verified');
      });
    });

};
