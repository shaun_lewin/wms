var mongoose = require('mongoose');
var Exchange = require('../app/models/exchange');

module.exports = {

  addExchange: function(req,res,next) {

      var exchange = new Exchange(),
          userId = req.decoded._id;

      exchange.lender   = userId;
      exchange.borrower = req.body.borrower;
      exchange.loanDate = Date.now();
      exchange.item     = mongoose.Types.ObjectId(req.body.itemId);
      //todo - write function to get exchange type
      exchange.type     = req.body.type;

      exchange.save(function(err) {

        if (err) return next(err);

        var _processed = false;

        _processed = exchange.process(req,res,next);       

      });
  },

  getExchange: function(req,res,next) {
      var query;

      if(req.query) {
        query = req.query || {};
      }

      Exchange.find(query, function(err,exchanges) {
        if(err) {
          throw err;
        }
        res.json(exchanges);
      });
  },

  updateExchange: function(req,res,next) {

    var values = {};
    for(var key in req.body) {
      values[key] = req.body[key];
    }

    Exchange.findOneAndUpdate({
      "_id":req.body._id
    },
    {
      "$set": values
    },
    function(err, exchange) {

      if(err) throw err;

      exchange.save(function(err) {
          if(err) throw err;

          res.json({
            success: true,
            message: "Exchange updated!"
          });
      });
    });
  },

  removeExchange: function(req,res,next) {

  }
}
