var User = require('../app/models/user');

module.exports = {

  createUser: function(req,res,next) {
    var user = new User();

     //Check if query params match user schema
     //todo - function to match user params to given schemsa
     user.username = req.body.username;
     user.password = req.body.password;
     user.email    = req.body.email;

     user.save(function(err) {
       //TOdo - error handler
         if(err) return next(err);
         res.json({message: 'User added!'});
     });
  },

  getUsers: function(req,res,next) {
    var query;

    if(req.query) {
        query = req.query || {};
    }

    User.find(query, function(err,users) {
      if(err) {
          throw err;
      }
      res.json(users);
    });
  },

  updateUser: function(req,res,next) {

      var values = {};
      for(var key in req.body) {
        values[key] = req.body[key];
      }

      User.findOneAndUpdate({
        "_id":req.body._id
      },
      {
        "$set": values
      },
      function(err, user) {

        console.log(user);
        if(err) throw err;

        user.save(function(err) {
            if(err) throw err;

            res.json({
              success: true,
              message: "User updated!"
            });
        });
      }
    )
  },

  deleteUser: function(req,res,next) {

  }
};
